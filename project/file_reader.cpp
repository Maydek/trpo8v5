#include "file_reader.h"
#include "constans.h"

#include <fstream>
#include <cstring>
#include <iostream>

date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ": ", &context);

    result.hour = atoi(str_number);

    str_number = strtok_s(NULL, ": ", &context);
    result.minutes = atoi(str_number);
    str_number = strtok_s(NULL, ": ", &context);
    result.second = atoi(str_number);
    return result;
}

void read(const char* file_name, member_maraphone* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            member_maraphone* item = new member_maraphone;
            file >> item->number;
            file >> item->member.last_name;
            file >> item->member.first_name;
            file >> item->member.middle_name;
            file >> tmp_buffer;
            item->start = convert(tmp_buffer);
            file >> tmp_buffer;
            item->finish = convert(tmp_buffer);
            file.read(tmp_buffer, 1);
            file.getline(item->club, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}