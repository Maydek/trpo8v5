#ifndef FILTER_H
#define FILTER_H

#include "member_maraphone.h"

member_maraphone** filter(member_maraphone* array[], int size, bool (*check)(member_maraphone* element), int& result_size);

bool check_club_member(member_maraphone* element);
bool check_member_by_result(member_maraphone* element);


#endif


